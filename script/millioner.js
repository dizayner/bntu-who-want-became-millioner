﻿function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

angular.module('millionerApp', [
  'ngRoute'
]) .config(function ($routeProvider) {
    $routeProvider
      .when('/game', {
        template: '<game></game>'
      })
      .otherwise('/game');
  });


angular.module('millionerApp').component('game', {
  template: `
   <div class="main">
   <div class="center">
     <h1>
     {{gameCtrl.question.text}}
   </h1>

    <div ng-repeat="(i,item) in gameCtrl.question.answers" ng-click="gameCtrl.selectAnswer(item, i)">
        <div
          class="button width100"
          ng-class="gameCtrl.isSelected(i) ? 'selected':''">
             {{item.text}} {{gameCtrl.resolvePercent(i)}}
           <div ng-if="gameCtrl.check5050(i)">Возможно это</div>
         </div>
    </div>

        <button
          class="button hint"
          ng-class="!gameCtrl.can5050() ? 'dissable':''"
          ng-click="gameCtrl.hint5050()"
          ng-disabled="!gameCtrl.can5050()">
            50/50
        </button>

        <button
          class="button hint"
          ng-class="!gameCtrl.canPoll() ? 'dissable':''"
          ng-click="gameCtrl.hintPoll()"
          ng-disabled="!gameCtrl.canPoll()">
           Опрос зала
        </button>

        <button
          class="button check-answer"
          ng-click="gameCtrl.checkAnswer()"
          ng-disabled="gameCtrl.state.selectedAnswerId === -1 ">
            Ответить
        </button>

         <div>
           Несгораемая сумма: {{gameCtrl.state.incombustibleAmount}} /
           Текущаю сумма: {{gameCtrl.state.currentAmount}} /
           Уровень: {{gameCtrl.currentLevel()}} /
           Вопрос {{gameCtrl.currentQuestion}} из {{gameCtrl.totalQuestion}}
          </div>
            
    </div>
   </div>
   `,

  controllerAs: 'gameCtrl',
  controller: function gameController(questionService) {

    var self = this;
    var levelstep = 2;
    var level = ["easy", "middle", "hard"];
    var uiLevel = ["легкий", "средний", "тяжелый"]

    
    self.currentLevel = function(){
      return uiLevel[self.state.currentLevel];
    }
    self.can5050 = function(){
      return self.state.hint5050.can;
    }
    
    self.check5050 = function(index){
      return self.state.hint5050.selectedAnswer[index];
    }
    
    self.hint5050 = function () {
      self.state.hint5050.can = false;
      var selectedAnswer = self.state.hint5050.selectedAnswer;

      self.question.answers.some(function (item, index) {
        if (item.correct) {
          selectedAnswer[index] = true;
          return true;
        }
        return false;
      });

      while (true) {
        var randomElement = getRandom(0, 3);

        if (selectedAnswer[0] == randomElement) {
          continue;
        } else {
          selectedAnswer[randomElement] = true;
          break;
        }
      }
    }

    self.canPoll = function(){
      return self.state.hintPoll.can;
    }
   
    self.hintPoll = function () {
      self.state.hintPoll.can = false;

      var percent = self.state.hintPoll.answerPercent;
      percent.push(Math.floor(Math.random(0, 50) * 50));
      percent.push(percent.push(50 - percent[0]));
      percent.push(Math.floor(Math.random(0, 50) * 50));
      percent.push(percent.push(50 - percent[2]));

      percent.sort(function (a, b) {
        return Math.random() - 0.5;
      })
    }
     
    self.resolvePercent = function(index){
      return self.state.hintPoll.answerPercent[index]
    }

    self.selectAnswer = function (answer, index) {
      self.state.selectedAnswer = answer;
      self.state.selectedAnswerId = index;
    }

    self.isSelected = function(index){
      return self.state.selectedAnswerId === index;
    }
    
    self.checkAnswer = function () {
      self.state.hintPoll.answerPercent = [];
      self.state.hint5050.selectedAnswer = {};

      if (self.state.selectedAnswer.correct) {
          handleCorrectAnswer();
      } else {
        alert("Вы проиграли. Ваша сумма:" + self.state.incombustibleAmount);
        start();
      }
    }

    self.$onInit = function () {
      start();
    }

   function start() {
      self.question = questionService.getNext(level[0]);
      self.totalQuestion = level.length*levelstep;
      self.currentQuestion = 0;
      self.state = {
        exclude: [],
        hintPoll: { can: true, answerPercent: [] },
        hint5050: { can: true, selectedAnswer: {} },
        currentAmount: 0,
        incombustibleAmount: 0,
        stepInCurrentLevel: 1,
        currentLevel: 0,
        selectedAnswer: '',
        selectedAnswerId: -1
      }
    }

   function handleCorrectAnswer() {
      self.state.currentAmount += 100 * (self.state.currentLevel + self.state.stepInCurrentLevel);
      self.currentQuestion++;
      
      if (self.state.stepInCurrentLevel === levelstep && self.state.currentLevel === 2) {
        alert("Вы победили!!! Сумма: " + self.state.currentAmount);
        self.start();

        return;
      }

      self.state.exclude.push(self.question.id);

      if (self.state.stepInCurrentLevel === 2) {
        self.state.stepInCurrentLevel = 1;
        self.state.currentLevel++;
        self.state.incombustibleAmount = self.state.currentAmount;
      } else {
        self.state.stepInCurrentLevel++;
      }
      goNext();
    }
    
    function goNext() {
      self.question = questionService.getNext(level[self.state.currentLevel], self.state.exclude);
      self.state.selectedAnswer = '';
      self.state.selectedAnswerId = -1;
    }
  }
});

angular
  .module('millionerApp')
  .factory('questionService', function () {

    var questionsStubs = [{
      id: 0,
      category: 'easy',
      text: "Лучший друг чебурашки",
      answers: [
        { text: "Голубой вертолет" },
        { text: "ДоналдДак" },
        { text: "Крокодил Гена", correct: true },
        { text: "Дядя Вася" }]
    },
      {
        id: 1,
        category: 'easy',
        text: "Кто вращается вокруг Земли",
        answers: [
          { text: "Луна", correct: true },
          { text: "Дом труба" },
          { text: "Колобок" },
          { text: "Дядя Вася" }]
      },
  	  	{
  	  	  id: 2,
  	  	  category: 'easy',
  	  	  text: "Форма колобка",
  	  	  answers: [
          { text: "Шар", correct: true },
          { text: "Бесформенный" },
          { text: "Куб" },
          { text: "Прямоугольник" }]
  	  	},
      {
        id: 3,
        category: 'middle',
        text: "Площадь квадрата",
        answers: [
          { text: "Сторона*Сторона", correct: true },
          { text: "Сторона*Высота" },
          { text: "Сторона*Основание*3" },
          { text: "Сторона+Сторона" }]
      },
      {
        id: 4,
        category: 'middle',
        text: "Столица Польши",
        answers: [
          { text: "Белосток" },
          { text: "Варшава", correct: true },
          { text: "Минск" },
          { text: "Познань" }]
      },
      {
        id: 5,
        category: 'middle',
        text: "Кто убил Лору Палмер?",
        answers: [
          { text: "Садовник" },
          { text: "Отец", correct: true },
          { text: "Дальнобойщик" },
          { text: "Космонват" }]
      },
      {
        id: 6,
        category: 'hard',
        text: "Язык JS является?",
        answers: [
          { text: "Сложный" },
          { text: "Компилируемый" },
          { text: "Хипстерский" },
          { text: "Интерпретируемый", correct: true }]
      },
      {
        id: 7,
        category: 'hard',
        text: "Работа не волк, а ворк. А волк это",
        answers: [
          { text: "Получать зарплату" },
          { text: "Ходить", correct: true },
          { text: "Спать" },
          { text: "Пропускать" }]
      },
    ];


    return {
      getNext: function (category, exclude) {
        var questions = questionsStubs
          .filter(function (question) {
            exclude = exclude || [];

            return question.category === category && exclude.some(function (id) {
              return id === question.id
            }) == false;
          })

        var returnQuestion = questions[getRandom(0, questions.length - 1)]

        return returnQuestion;
      }
    }

  });
